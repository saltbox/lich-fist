﻿using UnityEngine;
using System.Collections;



public class Player : Actor {
    int punchFrames = 12, kickFrames = 18, beamFrames = 45;
    GameObject punchHitbox, kickHitbox;
    ActorState lastState;
    PlayerAnimator animator;
    int lastMoveTimer;
	// Use this for initialization
	void Start () {
        health = 100;
		punchHitbox = transform.FindChild("Punch Hitbox").gameObject;
        kickHitbox = transform.FindChild("Kick Hitbox").gameObject;
        lastState = state;
        animator = GetComponent<PlayerAnimator>();
	} 

    void Update() {

        if (moveTimer <= 10)
        {
            if (Input.GetButtonDown("Jump"))
            {
                jumpTimer = 20;
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 7);
            }

            if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0 && moveTimer <= 0)
            {
                state = ActorState.Move;
            }
            else if (moveTimer <= 0)
            {
                state = ActorState.Stand;
            }

            if (Input.GetButtonDown("Punch"))
            {

                state = ActorState.Punch;
                moveTimer = punchFrames;
            }

            if (Input.GetButtonDown("Kick"))
            {
                state = ActorState.Kick;
                moveTimer = kickFrames;
            }

            if (Input.GetButtonDown("Beam"))
            {
                state = ActorState.Beam;
                moveTimer = beamFrames;
            }
        }
    }
	
	void FixedUpdate () {
        if (lastState != state || lastMoveTimer < moveTimer)
        {
            animator.ResetAnimations();
            ClearHitboxes();
        }

        if (state == ActorState.Move)
        {
            if (Input.GetAxis("Horizontal") > 0.001)
            {
                rigidbody2D.velocity = new Vector2(5, rigidbody2D.velocity.y);
                facingRight = true;
            }
            else if (Input.GetAxis("Horizontal") < -0.001)
            {
                rigidbody2D.velocity = new Vector2(-5, rigidbody2D.velocity.y);
                facingRight = false;
            }
        }
        else 
        {
            rigidbody2D.velocity = new Vector2(Mathf.Lerp(rigidbody2D.velocity.x, 0f, 0.25f), rigidbody2D.velocity.y);
        }

        if (facingRight)
        {
            transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
        }
        else
        {
            transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
        }

		if (state == ActorState.Punch) {
			if (moveTimer == 11) {
				punchHitbox.SetActive(true);
			}
			if (moveTimer == 4) {
				punchHitbox.SetActive(false);	
			}
		}

        if (state == ActorState.Kick)
        {
            if (moveTimer == 18)
            {
                kickHitbox.SetActive(true);
            }
            if (moveTimer == 8)
            {
                kickHitbox.SetActive(false);
            }
        }

        moveTimer--;
        jumpTimer--;
        lastState = state;
        lastMoveTimer = moveTimer;
	}

    void ClearHitboxes()
    {
        punchHitbox.SetActive(false);
        kickHitbox.SetActive(false);
    }
}
