﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour {
    int walkTimer;
    Player player;
    SpriteRenderer playerSprite;
    SpriteRenderer punchSprite, kickSprite;
    public Sprite stand, walk1, walk2;
    public Sprite punch1, punch2, punch3;
    public Sprite kick1, kick2, kick3;
	// Use this for initialization
	void Start () {
        player = GetComponent<Player>();
        playerSprite = GetComponent<SpriteRenderer>();
        punchSprite = transform.FindChild("Punch Sprite").GetComponent<SpriteRenderer>();
        kickSprite = transform.FindChild("Kick Sprite").GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        switch(player.state) {
            case ActorState.Move:
                walkTimer++;
                if (walkTimer >= 10)
                {
                    playerSprite.sprite = walk1;
                }
                else
                {
                    playerSprite.sprite = walk2;
                }

                if (walkTimer >= 20)
                {
                    walkTimer = 0;
                }
                break;
            case ActorState.Stand:
                playerSprite.sprite = stand;
                break;
            case ActorState.Punch:
                if (player.moveTimer == 12)
                {
                    punchSprite.sprite = punch1;
                }
                else if (player.moveTimer == 9)
                {
                    punchSprite.sprite = punch2;
                }
                else if (player.moveTimer == 7)
                {
                    punchSprite.sprite = punch3;
                }
                else if (player.moveTimer <= 1)
                {
                    punchSprite.sprite = null;
                }
                break;
            case ActorState.Kick:
                if (player.moveTimer == 18)
                {
                    kickSprite.sprite = kick1;
                }
                else if (player.moveTimer == 14)
                {
                    kickSprite.sprite = kick2;
                }
                else if (player.moveTimer == 12)
                {
                    kickSprite.sprite = kick3;
                }
                else if (player.moveTimer <= 1)
                {
                    kickSprite.sprite = null;
                }
                break;
            default:
                playerSprite.sprite = stand;
                break;
        }

	}

    public void ResetAnimations()
    {
        punchSprite.sprite = null;
        kickSprite.sprite = null;
    }
}
