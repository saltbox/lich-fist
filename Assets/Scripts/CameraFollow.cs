﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
    public GameObject target;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.position = new Vector3(
            Mathf.Lerp(transform.position.x, target.transform.position.x, 1f),
            Mathf.Lerp(transform.position.y, target.transform.position.y, 1f),
            -5);
	}
}
