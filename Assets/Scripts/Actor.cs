﻿using UnityEngine;
using System.Collections;

public enum ActorState { Stand, Move, Stagger, Punch, Kick, Beam }

public class Actor : MonoBehaviour {
    public float health;
    public ActorState state = ActorState.Stand;
    public bool jumping = false;
    public int moveTimer = 0, jumpTimer;
    public bool facingRight = true;

    public void Hit(int damage, int frames)
    {
        state = ActorState.Stagger;
        moveTimer = frames;
        health -= damage;
    }

    public void Hit(int damage, int frames, Vector2 push, Transform attacker)
    {
        Hit(damage, frames);
        if (attacker.transform.position.x < transform.position.x)
        {
            rigidbody2D.velocity = push;
        }
        else
        {
            rigidbody2D.velocity = new Vector2(-push.x, push.y);
        }
    }

    public void FixedUpdate()
    {
        if (health <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
