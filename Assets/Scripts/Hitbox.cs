﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HitType { Punch, Kick, Beam, Burst }

public class Hitbox : MonoBehaviour {

    List<GameObject> hitGameObjects = new List<GameObject>();
    public HitType hitType;

	// Use this for initialization
	void Start () {
        
	}

    void OnEnable()
    {
        hitGameObjects.Clear();
        Physics2D.IgnoreCollision(collider2D, transform.parent.collider2D);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit!");
        if (!hitGameObjects.Contains(other.gameObject))
        {
            hitGameObjects.Add(other.gameObject);
            Hit(other.gameObject);
            //hit 'em HIT 'EM hit 'em
        }
    }

    void Hit(GameObject gameObject)
    {
        switch (hitType)
        {
            case HitType.Punch:
                gameObject.GetComponent<Actor>().Hit(5, 15, new Vector2(1, 0), transform.parent);
                break;
            case HitType.Kick:
                gameObject.GetComponent<Actor>().Hit(8, 30, new Vector2(0, 7.5f), transform.parent);
                break;
            case HitType.Beam:
                gameObject.GetComponent<Actor>().Hit(20, 50);
                break;
            case HitType.Burst:
                gameObject.GetComponent<Actor>().Hit(20, 40);
                break;
        }
    }
}
